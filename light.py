"""Platform for sensor integration."""
from __future__ import annotations
import time
from datetime import (datetime,timedelta)

from homeassistant.components.light import (ATTR_BRIGHTNESS , PLATFORM_SCHEMA,
                                            LightEntity,SUPPORT_BRIGHTNESS)
from homeassistant.core import HomeAssistant
from homeassistant.helpers.entity_platform import AddEntitiesCallback
from homeassistant.helpers.typing import ConfigType, DiscoveryInfoType
from .tinxybulbapi import TinxyBulbAPI

# Log file ( FIXME: not a good idea )
f=open("/tmp/logfile","w")
f.write("cool!\n")
f.flush()

# Set the polling time (frequency of calling the update function)
SCAN_INTERVAL = timedelta(seconds=5)

def setup_platform(
    hass: HomeAssistant,
    config: ConfigType,
    add_entities: AddEntitiesCallback,
    discovery_info: DiscoveryInfoType | None = None
) -> None:
    """Set up the light platform."""
    add_entities([ExampleLight()])


class ExampleLight(LightEntity):
    """Representation of a Light."""

    def __init__(self) -> None:
        """Initialize an AwesomeLight.""
        # self.api_key = set api key
        # self.device_id = set device id
        self._light = TinxyBulbAPI(self.api_key,self.device_id)
        self._name = "Abhijith's Example Light"
        self._state = None
        self._brightness = None
        # Get the feature and set in the class 
        # FIXME: This way is obsolete find new way or go home.
        self._attr_supported_features = self.get_supported_features()


    @property
    def name(self) -> str:
        """Return the display name of this light."""
        return self._name

    @property
    def brightness(self):
        """Return the brightness of the light.
        This method is optional. Removing it indicates to Home Assistant
        that brightness is not supported for this light.
        """
        return self._brightness

    @property
    def is_on(self) -> bool | None:
        """Return true if light is on."""
        return self._state

    def turn_on(self, **kwargs: Any) -> None:
        """Instruct the light to turn on with an x brightness """
        # Just setting the brightness will turn on the bulb even if it's off
        brightness = kwargs.get(ATTR_BRIGHTNESS, 255)
        f.write(f"On_BTNS_KWARGS_DEBG:{kwargs},BNS:{brightness}\n")
        f.flush()
        # Equation value = (value/255)*100 where 255 is the max value (8bit)
        self._light.set_brightness(int(brightness/(2.55)));

    def turn_off(self, **kwargs: Any) -> None:
        """Instruct the light to turn off."""
        f.write("BULB_DBUG: It's off.. dont bother looking.\n")
        f.flush()
        # Turn off the bulb without setting any state, to preserve brightness
        self._light.set_state(False)

    def update(self) -> None:
        """Fetch new state data for this light.
        This is the only method that should fetch new data for Home Assistant.
        """
        status = self._light.get_status()
        self._state = status["state"]=="ON"
        self._brightness = int(status["brightness"])
        f.write(f"Brightness_DEBG:{self._brightness},Time:{datetime.now()}")
        f.write("\n")
        f.flush()
        # Reverse calculate the brightness value to set the slider properly
        # Which goes from 0<->255.
        self._brightness = (self._brightness*255)/100
        self._brightness=int(self._brightness)

    def get_supported_features(self) -> int:
        # Return that only supported feature is brightness
        return SUPPORT_BRIGHTNESS