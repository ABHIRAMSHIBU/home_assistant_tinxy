
import http.client
import json
import time

class TinxyBulbAPI:
    def __init__(self,auth_token,device_id) -> None:
        self._auth_token = auth_token
        self._device_id = device_id
        self.conn = http.client.HTTPSConnection("backend.tinxy.in")
        self.headers = {
            'Content-Type': 'application/json',
            'Authorization': f'Bearer {self._auth_token}'
        }
    
    def send_request(self,type,url,payload,headers=None):
        if(not headers):
            headers=self.headers
        self.conn.request(type,url,payload,headers)
        res = self.conn.getresponse()
        data = res.read()
        return data.decode("utf-8")

    def set_state(self,is_on: bool) -> None:
        url = f"/v2/devices/{self._device_id}/toggle"
        type= "POST"

        payload = json.dumps({
        "request": {
            "state": 1 if is_on else 0
        },
        "deviceNumber": 1
        })
        response = self.send_request(type,url,payload)
        print(response)

    def set_brightness(self,brightness) -> None:
        
        url = f"/v2/devices/{self._device_id}/toggle"
        type= "POST"

        payload = json.dumps({
        "request": {
            "brightness":str(brightness) 
        },
        "deviceNumber": 1
        })

        response = self.send_request(type,url,payload)
        print(response)

    def get_status(self) -> dict:
    #    conn.request("GET", "/v2/devices/62dba0c57cda03953de1cf44/state?deviceNumber=1", payload, headers)
        type = "GET"
        url = f"/v2/devices/{self._device_id}/state?deviceNumber=1"
        response = self.send_request(type,url,None)
        return json.loads(response)

